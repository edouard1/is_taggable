class Tag < ActiveRecord::Base
  has_many :taggings, :dependent => :destroy

  validates_presence_of :name
  validates_uniqueness_of :name, :scope => [:project_id, :kind]

  scope :with_name_like_and_kind_and_project_id, lambda { |name, kind, project_id| where("project_id = ? AND name like ? AND kind = ?", project_id, name, kind) }
  scope :of_kind,                 lambda { |kind| where(:kind => kind) }
  
  def decrement_taggings_count!
    self.taggings_count -= 1
    self.save
  end
  
  def increment_taggings_count!
    self.taggings_count += 1
    self.save
  end
end
